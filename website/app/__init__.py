# Mini Flask example app.
import os
from flask import Flask, g
from .database import pymongo
import subprocess

def create_app(test_config=None):
    # Create assets
    subprocess.run(
        "sass app/static/main.sass app/static/css/all.css",
        shell=True,
        check=True)

    # create app and configure
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY = 'a random string',
        WTF_CSRF_SECRET_KEY = 'a random string'
    )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    app.config["MONGO_URI"] = os.getenv("MONGO_URI")
    pymongo.init_app(app)

    # Loading blueprints
    from . import main
    app.register_blueprint(main.bp,url_prefix="/rosaespino")

    return app
