# Mini Flask example app.
from flask import (
        Blueprint, render_template, g, request, Markup
)
from flask_wtf import FlaskForm
from wtforms import StringField,IntegerField, BooleanField, FloatField, SelectField, TextAreaField
from wtforms.validators import DataRequired, NumberRange, Optional
from pymongo.collection import Collection, ReturnDocument
import time
import requests
import os
import sys
import uuid
import html
from datetime import datetime
from .model import Poem, UpdatedPoem
from .database import pymongo
from .objectid import PydanticObjectId
from flask_paginate import Pagination, get_page_parameter

bp = Blueprint("rosesp",
        __name__, 
        url_prefix="/",
        static_url_path='/static',
        static_folder='static'
        )
models= os.listdir("/models")
poems: Collection = pymongo.db.poems

class MyForm(FlaskForm):
    model = SelectField(u'Modelo', choices=[(os.path.join("/models",x),x) for x in models if not x.startswith(".")])
    title = StringField('Título', validators=[],default='las golondrinas')
    firstlines = TextAreaField('Primeras líneas', validators=[Optional()])
    min_length = IntegerField("Longitud mínima", validators=[NumberRange(min=50,max=500)],default=50)
    max_length = IntegerField("Longitud máxima", validators=[NumberRange(min=50,max=500)],default=100)
    num_beams = IntegerField("Número de beams", validators=[Optional(),NumberRange(min=1,max=5)])
    top_k = IntegerField("K top elementos durante muestreo", validators=[NumberRange(min=0,max=100)])
    top_p = FloatField("Probabilidad acumulada para  top elementos durante muestreo", validators=[Optional(),NumberRange(min=0.0,max=1.0)])
    no_repeat_ngram_size = IntegerField("Tamaño de grama sin repetición", validators=[Optional(),NumberRange(min=2,max=5)])
    num_return_sequences = IntegerField("Numero de poemas a regresar", validators=[Optional(),NumberRange(min=1,max=5)])
    temperature = FloatField("Temperatura", validators=[Optional(),NumberRange(min=0.0,max=1.0)])
    seed = IntegerField("Semilla aleatoria", validators=[Optional()])

@bp.before_request
def before_request():
    g.start_time = time.time()
    g.elapsed_time = lambda: time.time() - g.start_time

@bp.get("/")
def get_parameters():
    form = MyForm()
    if form.validate_on_submit():
        return redirect('/')
    return render_template('params.html', form=form)

@bp.post("/")
def show_gen_poems():
    json_ = {k:v for k,v in request.form.items() if v}
    r=requests.post("http://api:8000",json=json_)
    info = r.json()
    return render_template('show_poems.html', info=info,json_=json_)

@bp.post("/save")
def save_poem():
    poem= request.get_json()
    poem["date_created"] = datetime.utcnow()
    poem["date_last_edited"] = poem["date_created"]
    poem["uuid"]=str(uuid.uuid4())
    poem["public"]=True
    poem = Poem(**poem)
    insert_result=poems.insert_one(poem.to_bson())
    poem.id = PydanticObjectId(str(insert_result.inserted_id))
    return {"msg":"ok","uuid":poem.uuid}

@bp.get("/poem/<uuid>")
def show_poem(uuid):
    poem= poems.find_one({'uuid':uuid})
    poem_=html.unescape(poem['poem'])
    return render_template('show_poem.html',
            poem=poem_,
            uuid=uuid

            )

@bp.get("/edit/<uuid>")
def edit_poem(uuid):
    poem= poems.find_one({'uuid':uuid})
    poem_=html.unescape(poem['poem'])
    return render_template('edit_poem.html',
            poem=poem_.replace("<br>","\n"),
            uuid=uuid,
            poem_id=poem['_id']
            )

@bp.post("/update")
def update_poem():
    json_=request.get_json()
    poem_id = PydanticObjectId(str(json_['id']))
    poem= UpdatedPoem(**json_)
    poem.date_last_edited = datetime.utcnow()
    update_result = poems.update_one(
            {"_id":poem_id},
            {"$set": poem.to_json()},
            )
    updated_doc=None
    if update_result.modified_count == 1:
        updated_doc = poems.find_one({"_id": poem_id})
    if updated_doc:
        return Poem(**updated_doc).to_json()
    else:
        return "Error"


@bp.get("/poems")
def list_poems():
    page = int(request.args.get("page", type=int, default=1))
    per_page = 10  # A const value.

    cursor = poems.find().sort("date_created").skip(per_page * (page - 1)).limit(per_page)

    count = poems.count_documents({})

    pagination = Pagination(page=page, total=count, record_name='poems',per_page_parameter=per_page)
    
    return render_template("list_poems.html",
            poems= [Poem(**doc).to_json() for doc in cursor],
            pagination = pagination)





