# hf_website

Esqueleto de proyecto para la generación de texto basado en un modelo entrenado

## Arquitectura del sistema

![Arquitectura del sistema](architecture.svg?raw=true "Arquitectura del sistema")


## Dependencias

* Docker
* Docker-compose

# Ejecución modo _development_

Esta ejecución corre de forma local

## Construir la imagen

    docker-compose -f docker-compose.yml -f docker-compose.dev.yml build

## Ejecutar la imagen

    docker-compose -f docker-compose.yml -f docker-compose.dev.yml up

## Acceder a servicio

Ir al navegador e ingresar a esta dirección  http://0.0.0.0:8000


# Ejecución modo _production_

Esta ejecución es para ejecutarse en un servidor de producción

## Configurar el ambiente 

Copiar el archivo _.env.example_ a _.env_ y llenar la información con los datos
correctos para que corra en el ambiente 

## Construir la imagen

    docker-compose -f docker-compose.yml -f docker-compose.dev.yml build

## Ejecutar la imagen

    docker-compose -f docker-compose.yml -f docker-compose.dev.yml up

