# Mini fastapi example app.
from typing import Optional
from pydantic import BaseModel
from fastapi import FastAPI, Request
from functools import lru_cache
from .ml_model import generate
import time
from html import escape

# Check for more options: https://huggingface.co/transformers/main_classes/model.html?highlight=generate#transformers.TFPreTrainedModel.generate
# For an explanation check: https://huggingface.co/blog/how-to-generate
def collect_args(**args):
    # Validating
    def clip_value(value,min_value,max_value):
        if value in args:
            if args[value]:
                args[value]=min(args[value],max_value)
                args[value]=max(args[value],min_value)
            else:
                del args[value]
    clip_value('max_length',51,500)
    clip_value('min_length',50,args['max_length']-1)
    clip_value('num_beams',1,5)
    clip_value('no_repeat_ngram_size',2,5)
    clip_value('num_return_sequences',1,5)
    clip_value("top_k",0,100)
    clip_value("top_p",0.0,1.0)
    clip_value("temperature",0.0,1.0)

    return args

class Arguments(BaseModel):
    model: str = "__DEFAULT__"
    title: str = "las golondrinas"
    firstlines: Optional[str] = None
    max_length: int = 150
    min_length: int = 50
    num_beams: Optional[int] =None
    top_k: Optional[int] = None
    top_p: Optional[float]  = None
    do_sample: bool = True
    early_stopping: bool = True
    no_repeat_ngram_size: Optional[int] = None
    num_return_sequences: Optional[int] = None
    temperature: Optional[float] = None
    seed: Optional[int] = None


def create_app(test_config=None):
    # Arrange configuration
    from . import config
    @lru_cache()
    def get_settings():
        return config.Settings()
    setting=get_settings()

    # create app
    app = FastAPI()


    # Check for more options: https://huggingface.co/transformers/main_classes/model.html?highlight=generate#transformers.TFPreTrainedModel.generate
    # For an explanation check: https://huggingface.co/blog/how-to-generate

    # Call by arguments: api/?max_length=200&num_beams=2&top_p=0.95&seed=1337
    @app.get("/")
    def generate_poem(
            title: str = "las golondrinas",
            firstlines: Optional[str] = None,
            model: str = "__DEFAULT__",
            max_length: int = 150,
            min_length: int = 50,
            num_beams: Optional[int] = None,
            top_k: Optional[int] = None,
            top_p: Optional[float]  = None,
            do_sample: Optional[bool] = None,
            early_stopping: Optional[bool] = None,
            no_repeat_ngram_size: Optional[int] = None,
            num_return_sequences: Optional[int] = None,
            temperature: Optional[float] = None,
            seed: Optional[int] = None
            ):
        #  Time
        start_time = time.time()
        elapsed_time = lambda: time.time() - start_time
        # Recovering parameters from ULR
        title=title.strip()
        model=model.strip() if model else None
        if not model:
            return {"message":f"No model selected"}
        firstlines=firstlines.strip() if firstlines else None
        args=collect_args(
            max_length=max_length,
            min_length=min_length,
            num_beams=num_beams,
            top_k=top_k,
            top_p=top_p,
            do_sample=do_sample,
            early_stopping=early_stopping,
            no_repeat_ngram_size=no_repeat_ngram_size,
            num_return_sequences=num_return_sequences,
            temperature=temperature,
            )
        if len(prompt)==0:
            return {"message":f"{args['key']} could not be changed to {args['type']}"}
        poems=generate(model,title,firstlines,seed,**args)
        res={
            "poems": [poem for poem in poems],
            "title": escape(title),
            "elapsed_time_seconds": f'{elapsed_time():2.3f}',
            "args":args
        }
        if firstlines: res['firstlines']=firstlines
        if model: res['model']=firstlines
        if seed: res['seed']=seed
        return res

    # with a json: curl -X POST -H 'Content-Type: application/json' http://127.0.0.1:5000/api/ -d '{"max_length":200, "num_beams":2,"top_p":0.95, "seed":1337}'
    @app.post("/")
    def json_generate_poem(args: Arguments):
        #  Time
        start_time = time.time()
        elapsed_time = lambda: time.time() - start_time
        title=args.title.strip()
        model=args.model.strip() if args.model else None
        if not model:
            return {"message":f"No model selected"}
        firstlines=args.firstlines.strip() if args.firstlines else None
        seed=args.seed if args.seed else None
        args=collect_args(
            max_length=args.max_length,
            min_length=args.min_length,
            num_beams=args.num_beams,
            top_k=args.top_k,
            top_p=args.top_p,
            do_sample=args.do_sample,
            early_stopping=args.early_stopping,
            no_repeat_ngram_size=args.no_repeat_ngram_size,
            num_return_sequences=args.num_return_sequences,
            temperature=args.temperature,
            )
        if len(title)==0:
            return {"message":f"No title provided"}
        poems=generate(model,title,firstlines,seed,**args)
        res={
            "poems": [poem for poem in poems],
            "title": escape(title),
            "elapsed_time_seconds": f'{elapsed_time():2.3f}',
            "args":args
        }
        if firstlines: res['firstlines']=firstlines
        if model: res['model']=firstlines
        if seed: res['seed']=seed
        return res
    return app

app=create_app()
