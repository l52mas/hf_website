from pydantic import BaseSettings
import os

class Settings(BaseSettings):
    app_name: str = "Rosa Espino API"
    model_name_: str = "/models/pass_2"

    class Config:
        env_file = ".env"
