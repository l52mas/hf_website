# Loads the ML model
from transformers import pipeline, set_seed

generator=None
models={}

def init_generator(model, tokenizer=None):
    global models
    if model in models:
        return models[model]
    if tokenizer is None:
        tokenizer=model
    generator=pipeline('text-generation',model=model, tokenizer=tokenizer,)
    models[model]=generator
    if len(models)==1:
        models["__DEFAULT__"]=generator
    return generator


def filter_poem(poem):
    return poem.replace(" ~< ","\n\n").replace(" -- ","\n").replace(" <- ","\n").replace("<- ","\n").replace(" -> ","\n").replace("~ ","").replace(" >< ","\n\n").replace(" <> ","\n").replace(" ---- ","\n\n").replace(" >","\n\n").replace("-->","\n\n").replace(" -<< ", "\n").strip()


def generate(model,title,firstlines,seed,**args):
    global models
    init_generator(model)
    if seed: set_seed(seed)
    prompt=f" ~ {title.upper()} ~"
    if firstlines:
        lines=firstlines.split("\n")
        if len(lines) >0:
            prompt+=f"< {lines[0]} <"
        if len(lines) >2:
            for line in lines[1:-1]:
                prompt+=f"- {line} -"
        if len(lines) >1:
            prompt+=f"> {lines[-1]} >"

    texts=models[model](prompt,**args)
    return [filter_poem(text['generated_text']) for text in texts]

