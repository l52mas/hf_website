from transformers import pipeline
import requests
import zipfile
import os

if os.environ["MODEL_NAME"] in ["datificate/gpt2-small-spanish"]:
    model=os.environ["MODEL_NAME"]
    model_text = pipeline('text-generation',model=model, tokenizer=model,)
elif os.environ["MODEL_NAME"].startswith("http"):
    r = requests.get(os.environ["MODEL_NAME"], stream=True)
    with open(os.path.join("/models","model.zip"), 'wb') as fd:
        for chunk in r.iter_content(chunk_size=1024):
            fd.write(chunk)
    with zipfile.ZipFile(os.path.join("/models","model.zip"),"r") as zip_ref:
        zip_ref.extractall("/models")

print(f"Model {os.environ['MODEL_NAME']} ready")
